import React, {Component} from 'react';
import './App.scss';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import axios from 'axios'
import {Tooltip, Brush} from 'recharts';
import BarChart from "recharts/lib/chart/BarChart";
import XAxis from "recharts/lib/cartesian/XAxis";
import Bar from "recharts/lib/cartesian/Bar";
import YAxis from "recharts/lib/cartesian/YAxis";
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import moment from "moment";


export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            data: [],
            error: ''
        }
    }

    componentDidMount() {
        axios.get('https://public.ecologi.com/trees').then(res => {
            const data = res.data;
            const calendarData = [];
            const today = moment();
            let date = moment('2019-06-03'); // First Insta post about beta launch... we'll start from here.
            // Generate an empty array of dates from the date above to today, to fill in gaps/days with no trees.
            while (date.isSameOrBefore(today)) {
                let dateString = date.format('Y-MM-DD');
                calendarData[dateString] = {
                    dateString,
                    treeCount: 0,
                }
                date.add(1, 'day');
            }
            // Loop through the records to fill in/add up the number of trees for each day.
            data.forEach(receipt => {
                let dateString = receipt.createdAt.slice(0, 10);
                if (calendarData[dateString]) {
                    calendarData[dateString].treeCount += receipt.value;
                }
            });

            this.setState({data: calendarData, loading: false})
        }).catch(err => {
            this.setState({loading: false, error: err.message});
        })

    }

    render() {
        const tooltipDateFormat = (date) => {
                return moment(date).format('Do MMM Y')
        }
        const tooltipTreeFormat = (value) => {
                return [value.toLocaleString(), 'Trees Planted']
        }
        const brushDateFormat = (date) => {
            return moment(date).format('Do MMM Y')
        }
        const MyCustomChart = ({data}) => {
            const monthTickFormatter = (tick) => {
                const date = new moment(tick);

                return date.format('MMM Y');
            };
            return (
                <ResponsiveContainer>
                    <BarChart data={Object.values(data)} margin={{top: 25, right: 25, bottom: 45, left: 25}}>
                        <XAxis label={{value:"Date", position: "insideBottom", offset: -15}} dataKey="dateString" scale={'band'} tickFormatter={monthTickFormatter} minTickGap={50}/>
                        <YAxis label={{ value: "Trees Planted", angle: -90, position: 'insideLeft', offset: -15}}/>
                        <Bar dataKey="treeCount" fill="#0c9e77"/>
                        <Tooltip formatter={tooltipTreeFormat} labelFormatter={tooltipDateFormat}/>
                        <Brush dataKey={'dateString'} height={20} gap={1} tickFormatter={brushDateFormat}/>
                    </BarChart>
                </ResponsiveContainer>

            );
        }

        // This is slightly messy but was just making sure there was some loading/error states super quickly!
        let chartContainer = <div className={'chart-container'}>
            <div className={'info-box'}>
                <b>Psst!</b><p>Drag the blue slider under the graph to filter the date range.</p>
            </div>
            <MyCustomChart data={this.state.data}/>
        </div>;

        if(this.state.error) {
            chartContainer = <div className={'chart-container'}><h3 className={'placeholder-text'}>Oops! Looks like there was an issue loading this data.</h3></div>
        }
        if(this.state.loading) {
            chartContainer = <div className={'chart-container'}><h3 className={'placeholder-text'}>Loading data...</h3></div>
        }

        return <div className={'page-container'}>
            <div className={'dashboard-container'}>
                <Header/>
                <Container fluid className={'main-content'}>
                    <Row>
                        <Col style={{height: '75vh', padding: '0 4rem'}}>
                            <h2 className={'chart-title'}>Trees planted per day since launch</h2>
                            {chartContainer}
                        </Col>
                    </Row>
                </Container>
                <section className={'footer'}>
                    {/* Space for footer links etc. */}
                </section>
            </div>
        </div>;
    }
}

const Header = () => {
    return <header>
        <a href={'https://ecologi.com/'} title={'Go to the Ecologi website'} target={'_blank'}></a>
    </header>
}
