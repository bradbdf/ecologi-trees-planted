# Ecologi Tree Planting Stats

---
## Requirements

Just yarn or npm.


## Getting up and running

This project uses [Parcel](https://parceljs.org/) to keep it lightweight, so should just be a case of cloning the repo then:

    $ yarn install
or 
    
    $ npm install

## Running the project

    $ yarn dev
or 
    
    $ npm run dev
    
then the dev server will be running at [http://localhost:1234/](http://localhost:1234/)

## Simple build for production

(Not needed but is just a shortcut for the default build command in Parcel)

    $ yarn build
or 
    
    $ npm run build
